package pe.olympus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class OlsmdwreapiApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(OlsmdwreapiApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(OlsmdwreapiApplication.class);
	}

}

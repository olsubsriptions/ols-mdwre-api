package pe.olympus.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.web.client.RestTemplate;

@Configuration
@EnableJms
public class AppConfig {
	
//	@Bean
//	public Queue monetaRequestQueue() {
//		return new ActiveMQQueue(propertiesConfig.getMonetaRequestQueue());
//	}
//
//	@Bean
//	public Queue monetaResponseQueue() {
//		return new ActiveMQQueue(propertiesConfig.getMonetaResponseQueue());
//	}
	
//	@Bean
//	public DefaultJmsListenerContainerFactory alerterContainerFactory() throws IOException{
//		DefaultJmsListenerContainerFactory jmsFactory = new DefaultJmsListenerContainerFactory();
//		jmsFactory.setSessionTransacted(false);
//		jmsFactory.setSessionAcknowledgeMode(ActiveMQSession.INDIVIDUAL_ACKNOWLEDGE);		
//		jmsFactory.setConnectionFactory(activeMQConnectionFactory());
//		return jmsFactory;
//	}
//	
//	@Bean
//	public ActiveMQConnectionFactory activeMQConnectionFactory() throws IOException {
//		ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory();
////		factory.setMaxThreadPoolSize(Integer.parseInt(propertiesConfig.getThreadPool()));
//		factory.setBrokerURL(propertiesConfig.getBrokerUrl());	
//		return factory;
//	}
//	
//	@Bean
//	public JmsTemplate jmsTemplate() throws IOException {
//		return new JmsTemplate(activeMQConnectionFactory());
//	}
//	
//	@Bean 
//	public Connection activeMQConnection() throws JMSException {
//		ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
//		connectionFactory.setBrokerURL(propertiesConfig.getBrokerUrl());
//	    Connection connection = connectionFactory.createConnection();
//	    return connection;
//	} 	 

	@Bean
	public RestTemplate resTemplate() {
		return new RestTemplate();
	}

}

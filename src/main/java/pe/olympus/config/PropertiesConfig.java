package pe.olympus.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PropertiesConfig {
	
	@Value("${subscription.url}")
	private String subscriptionUrl;
	
	@Value("${alerter.queue}")
	private String alerterQueue;
	
	@Value("${queue.broker.port}")
	private String queuePort;

	public String getSubscriptionUrl() {
		return subscriptionUrl;
	}

	public void setSubscriptionUrl(String subscriptionUrl) {
		this.subscriptionUrl = subscriptionUrl;
	}

	public String getAlerterQueue() {
		return alerterQueue;
	}

	public void setAlerterQueue(String alerterQueue) {
		this.alerterQueue = alerterQueue;
	}

	public String getQueuePort() {
		return queuePort;
	}

	public void setQueuePort(String queuePort) {
		this.queuePort = queuePort;
	}
}

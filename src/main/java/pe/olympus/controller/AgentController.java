package pe.olympus.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import pe.olympus.model.AgentDTO;
import pe.olympus.model.Response;
import pe.olympus.service.QueueService;
import pe.olympus.service.SubscriptionService;

@RestController
@RequestMapping("/")
public class AgentController {
	
	private SubscriptionService subscriptionService;
	private QueueService queueService;
	
	@Autowired
	public AgentController(SubscriptionService subscriptionService,
			QueueService queueService) {
		this.subscriptionService = subscriptionService;
		this.queueService = queueService;
	}
	
	@PostMapping ("getMessage")	
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<List<AgentDTO>> getMessage(@RequestBody AgentDTO agentDTO){
		
		List<AgentDTO> listAgentDTO = null;
		Response response = subscriptionService.verifySubscription(agentDTO);
		if (response.getStatus()) {
			listAgentDTO = queueService.getQueueMessage(response.getAppInstance(), agentDTO);
		}		
		return new ResponseEntity<List<AgentDTO>>(listAgentDTO, HttpStatus.OK);
	}
	
	@PostMapping ("sendMessage")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<Response> sendMessage(@RequestBody AgentDTO agentDTO) {
		
		Response response = subscriptionService.verifySubscription(agentDTO);
		if(response.getStatus()) {
			response = queueService.sendQueueMessage(response.getAppInstance(), agentDTO, false);
		}		
		return new ResponseEntity<Response>(response, HttpStatus.OK);		
	}
	
	@PostMapping ("sendAlert")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<Response> sendAlert(@RequestBody AgentDTO agentDTO) {
		
		Response response = subscriptionService.verifySubscription(agentDTO);
		if(response.getStatus()) {
			response = queueService.sendQueueMessage(response.getAppInstance(), agentDTO, true);
		}		
		return new ResponseEntity<Response>(response, HttpStatus.OK);		
	}
}

package pe.olympus.model;

public class AppSubsInstance {

	private String d0731; // ouid
	private String d0732; // instanceQueue
	private String i7071; // instanceId
	private String w0791; // appSubsInstanceId
	
	public String getD0731() {
		return d0731;
	}
	public void setD0731(String d0731) {
		this.d0731 = d0731;
	}
	public String getD0732() {
		return d0732;
	}
	public void setD0732(String d0732) {
		this.d0732 = d0732;
	}
	public String getI7071() {
		return i7071;
	}
	public void setI7071(String i7071) {
		this.i7071 = i7071;
	}
	public String getW0791() {
		return w0791;
	}
	public void setW0791(String w0791) {
		this.w0791 = w0791;
	}
	@Override
	public String toString() {
		return "AppSubsInstance [d0731=" + d0731 + ", d0732=" + d0732 + ", i7071=" + i7071 + ", w0791=" + w0791 + "]";
	}	
}

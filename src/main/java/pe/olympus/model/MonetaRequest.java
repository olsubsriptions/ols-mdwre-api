package pe.olympus.model;

public class MonetaRequest {

	private int mnt_id;
	private MonetaRequestParams mnt_params;
	private String prov_name;
	
	public int getMnt_id() {
		return mnt_id;
	}
	public void setMnt_id(int mnt_id) {
		this.mnt_id = mnt_id;
	}	
	public MonetaRequestParams getMnt_params() {
		return mnt_params;
	}
	public void setMnt_params(MonetaRequestParams mnt_params) {
		this.mnt_params = mnt_params;
	}
	public String getProv_name() {
		return prov_name;
	}
	public void setProv_name(String prov_name) {
		this.prov_name = prov_name;
	}	
}


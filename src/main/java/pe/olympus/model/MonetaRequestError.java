package pe.olympus.model;

public class MonetaRequestError {
	private String mnt_des;
	private int mnt_rc;
	
	public String getMnt_des() {
		return mnt_des;
	}
	public void setMnt_des(String mnt_des) {
		this.mnt_des = mnt_des;
	}
	public int getMnt_rc() {
		return mnt_rc;
	}
	public void setMnt_rc(int mnt_rc) {
		this.mnt_rc = mnt_rc;
	}	
}

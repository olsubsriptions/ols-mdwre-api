package pe.olympus.model;

public class MonetaRequestParams {
	
	private String mnt_cipher;
	private String mnt_hhuid;
	
	public String getMnt_cipher() {
		return mnt_cipher;
	}
	public void setMnt_cipher(String mnt_cipher) {
		this.mnt_cipher = mnt_cipher;
	}
	public String getMnt_hhuid() {
		return mnt_hhuid;
	}
	public void setMnt_hhuid(String mnt_hhuid) {
		this.mnt_hhuid = mnt_hhuid;
	}
}

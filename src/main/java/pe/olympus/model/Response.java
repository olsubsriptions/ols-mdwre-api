package pe.olympus.model;

public class Response {
	
	private boolean status;
	private AppInstance appInstance;
	private String message;
	
	public boolean getStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public AppInstance getAppInstance() {
		return appInstance;
	}
	public void setAppInstance(AppInstance appInstance) {
		this.appInstance = appInstance;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public String toString() {
		return "Response [status=" + status + ", appInstance=" + appInstance + ", message=" + message + "]";
	}	
}

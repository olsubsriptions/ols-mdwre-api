package pe.olympus.model;

public class User {
	
	private String g6731;
	private String g6732;
	private String g6733;
	private String b4321;
	
	public String getG6731() {
		return g6731;
	}
	public void setG6731(String g6731) {
		this.g6731 = g6731;
	}
	public String getG6732() {
		return g6732;
	}
	public void setG6732(String g6732) {
		this.g6732 = g6732;
	}
	public String getG6733() {
		return g6733;
	}
	public void setG6733(String g6733) {
		this.g6733 = g6733;
	}
	public String getB4321() {
		return b4321;
	}
	public void setB4321(String b4321) {
		this.b4321 = b4321;
	}
}

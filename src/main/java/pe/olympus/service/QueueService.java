package pe.olympus.service;

import java.util.ArrayList;
import java.util.List;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.ActiveMQSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import pe.olympus.config.PropertiesConfig;
import pe.olympus.constants.ResponseMessages;
import pe.olympus.model.AgentDTO;
import pe.olympus.model.AppInstance;
import pe.olympus.model.Response;

@Service
public class QueueService {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private PropertiesConfig propertiesConfig;
	@Autowired
	private Gson gsonTemplate;
	
	public List<AgentDTO> getQueueMessage (AppInstance appInstance, AgentDTO agentDTO) {		
		
			return this.consumeMessage(appInstance, agentDTO);				
	}
	
	public Response sendQueueMessage (AppInstance appInstance, AgentDTO agentDTO, boolean isAlert) {
		
			return this.produceMessage(appInstance, agentDTO, isAlert);		
	}
	
	public Response produceMessage(AppInstance appInstance, AgentDTO agentDTO, boolean isAlert) {
		
		try {		
				 
			Connection activeMQConnection = activeMQConnection(appInstance);
			activeMQConnection.start();
			Session session = activeMQConnection.createSession(false, ActiveMQSession.AUTO_ACKNOWLEDGE);			
			String queueName = isAlert ? propertiesConfig.getAlerterQueue() : appInstance.getD0732() + ".RES" ;						
			Destination destination = session.createQueue(queueName);
			MessageProducer producer = session.createProducer(destination);
			logger.debug("QueueService.produceMessage #queueName : {}", queueName);
			
			logger.info("QueueService.produceMessage : starting to send message to {}", queueName);
			logger.debug("QueueService.produceMessage #message to send : {}", agentDTO.getCipher());
			TextMessage message = session.createTextMessage(agentDTO.getCipher());
			message.setJMSCorrelationID(agentDTO.getTransaction_id());
			
			producer.send(message);
			producer.close();
			activeMQConnection.close();
			logger.info("QueueService.produceMessage : connection to queue {} is closed", queueName);
			
			Response response = new Response();
			response.setStatus(true);
			response.setMessage(ResponseMessages.SEND_MESSAGE_EXITOSO);
			
			logger.info("QueueService.produceMessage : sending messages to {} has finished", queueName);
			logger.debug("QueueService.produceMessage #response : {}", gsonTemplate.toJson(response));
			logger.info("QueueService.produceMessage : produceMessage() is done");
			return response;
		} catch (Exception e) {
			
			logger.error("QueueService.produceMessage #error : {}", e.getMessage());
			Response response = new Response();
			response.setStatus(false);
			response.setMessage(e.getMessage());
			return response;
		}
	}	
	
	public List<AgentDTO> consumeMessage(AppInstance appInstance, AgentDTO agentDTO){
		
		try {
			List<AgentDTO> listAgentDTO = new ArrayList<AgentDTO>();
			Connection activeMQConnection = activeMQConnection(appInstance);
			
			activeMQConnection.start();
	        Session session = activeMQConnection.createSession(false, ActiveMQSession.AUTO_ACKNOWLEDGE);
	        String queueName = appInstance.getD0732() + ".REQ";
			Destination destination = session.createQueue(queueName);
			MessageConsumer consumer = session.createConsumer(destination);			
	        
			logger.info("QueueService.consumeMessage : starting to get messages from {}", queueName);
	        while (true) {     
	            Message message = consumer.receive(5000); 
	            if (message instanceof TextMessage) {
	            	AgentDTO newAgentDTO = new AgentDTO();
	                TextMessage textMessage = (TextMessage) message;
	                newAgentDTO.setTransaction_id(textMessage.getJMSCorrelationID());
	                newAgentDTO.setCipher(textMessage.getText());
	                newAgentDTO.setIndex_key(agentDTO.getIndex_key());
	                newAgentDTO.setWorking_application_key(agentDTO.getWorking_application_key());
	                listAgentDTO.add(newAgentDTO);	                
	            }
	            else{
	            	logger.info("QueueService.consumeMessage : messages obtained {}", listAgentDTO.size());
	            	logger.info("QueueService.consumeMessage : queue {} is empty", queueName);
	                consumer.close();
	                activeMQConnection.close();
	                logger.info("QueueService.consumeMessage : connection to queue {} is closed", queueName);
	                break;
	            }
	        }
	        
	        logger.info("QueueService.consumeMessage : getting messages from {} has finished", queueName);
	        logger.debug("QueueService.consumeMessage #listAgentDTO : {}", gsonTemplate.toJson(listAgentDTO));
	        logger.info("QueueService.consumeMessage : consumeMessage() is done");
	        return listAgentDTO;
			
		} catch (Exception e) {
			logger.error("QueueService.consumeMessage #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}		
	}
	
	public Connection activeMQConnection(AppInstance appInstance) throws JMSException {
		ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
		connectionFactory.setBrokerURL("tcp://" + appInstance.getI7072() + ":" + propertiesConfig.getQueuePort());
	    Connection connection = connectionFactory.createConnection();
	    logger.info("QueueService.activeMQConnection : success");
	    logger.debug("QueueService.activeMQConnection #connection : {}", connection);
	    logger.info("QueueService.activeMQConnection : activeMQConnection() is done");
	    return connection;
	} 	

}

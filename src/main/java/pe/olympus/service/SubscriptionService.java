package pe.olympus.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import pe.olympus.config.PropertiesConfig;
import pe.olympus.model.AgentDTO;
import pe.olympus.model.Response;

@Service
public class SubscriptionService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private RestTemplate restTemplate;	
	private PropertiesConfig propertiesConfig;
	private Gson gsonTemplate;
	
	@Autowired
	public SubscriptionService(RestTemplate restTemplate,
			 PropertiesConfig propertiesConfig,
			 Gson gsonTemplate) {
		this.restTemplate = restTemplate;
		this.propertiesConfig = propertiesConfig;
		this.gsonTemplate = gsonTemplate;
	}
		
	public Response verifySubscription(AgentDTO agentDTO) {
		try {
			String url = propertiesConfig.getSubscriptionUrl() + "/applications/subscriptions/verify"
					+ "?indexKey=" + agentDTO.getIndex_key()
					+ "&workingApplicationKey=" + agentDTO.getWorking_application_key();
			logger.debug("SubscriptionService.verifySubscription : connecting to {}", url);
			
			Response response = restTemplate.getForObject(url, Response.class);
			
			logger.info("SubscriptionService.verifySubscription : verifySubscription() is done > {}", response.getMessage());
			logger.debug("SubscriptionService.verifySubscription #response : {}", gsonTemplate.toJson(response));
			return response;
				
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
	}
}
